/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.weather

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

interface IObservationRepository {
  fun load(): Flow<List<ObservationModel>>
  suspend fun refresh()
  suspend fun clear()
}

class ObservationRepository(
  private val db: ObservationDatabase,
  private val remote: ObservationRemoteDataSource
) : IObservationRepository {
  override fun load(): Flow<List<ObservationModel>> = db.observationStore()
    .load()
    .map { entities -> entities.map { it.toModel() }
  }

  override suspend fun refresh() {
    db.observationStore().save(convertToEntity(remote.getCurrentObservation()))
  }

  override suspend fun clear() = db.observationStore().clear()

  private fun convertToEntity(response: ObservationResponse): ObservationEntity {
    when {
      response.properties.temperature.unitCode != "unit:degC" ->
        throw IllegalStateException(
          "Unexpected temperature unit: ${response.properties.temperature.unitCode}"
        )
      response.properties.windDirection.unitCode != "unit:degree_(angle)" ->
        throw IllegalStateException(
          "Unexpected windDirection unit: ${response.properties.windDirection.unitCode}"
        )
      response.properties.windSpeed.unitCode != "unit:m_s-1" ->
        throw IllegalStateException(
          "Unexpected windSpeed unit: ${response.properties.windSpeed.unitCode}"
        )
      response.properties.barometricPressure.unitCode != "unit:Pa" ->
        throw IllegalStateException(
          "Unexpected barometricPressure unit: ${response.properties.barometricPressure.unitCode}"
        )
    }

    return ObservationEntity(
      id = response.id,
      icon = response.properties.icon,
      timestamp = response.properties.timestamp,
      temperatureCelsius = response.properties.temperature.value,
      windDirectionDegrees = response.properties.windDirection.value,
      windSpeedMetersSecond = response.properties.windSpeed.value,
      barometricPressurePascals = response.properties.barometricPressure.value
    )
  }
}

data class ObservationModel(
  val id: String,
  val timestamp: String,
  val icon: String,
  val temperatureCelsius: Double?,
  val windDirectionDegrees: Double?,
  val windSpeedMetersSecond: Double?,
  val barometricPressurePascals: Double?
)
