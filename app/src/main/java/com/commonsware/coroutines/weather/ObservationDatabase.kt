/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.weather

import android.content.Context
import androidx.annotation.NonNull
import androidx.room.*
import kotlinx.coroutines.flow.Flow

private const val DB_NAME = "weather.db"

@Entity(tableName = "observations")
data class ObservationEntity(
  @PrimaryKey @NonNull val id: String,
  val timestamp: String,
  val icon: String,
  val temperatureCelsius: Double?,
  val windDirectionDegrees: Double?,
  val windSpeedMetersSecond: Double?,
  val barometricPressurePascals: Double?
) {
  fun toModel() = ObservationModel(
    id = id,
    timestamp = timestamp,
    icon = icon,
    temperatureCelsius = temperatureCelsius,
    windDirectionDegrees = windDirectionDegrees,
    windSpeedMetersSecond = windSpeedMetersSecond,
    barometricPressurePascals = barometricPressurePascals
  )
}

@Dao
interface ObservationStore {
  @Query("SELECT * FROM observations ORDER BY timestamp DESC")
  fun load(): Flow<List<ObservationEntity>>

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  suspend fun save(entity: ObservationEntity)

  @Query("DELETE FROM observations")
  suspend fun clear()
}

@Database(entities = [ObservationEntity::class], version = 1)
abstract class ObservationDatabase : RoomDatabase() {
  abstract fun observationStore(): ObservationStore

  companion object {
    fun create(context: Context) =
      Room.databaseBuilder(context, ObservationDatabase::class.java, DB_NAME)
        .build()
  }
}