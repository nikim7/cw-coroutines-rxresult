## Elements of Kotlin Coroutines

[*Elements of Kotlin Coroutines*](https://commonsware.com/Coroutines/)
is a book focusing on Kotlin's native reactive programming system.

Among other things, this book contains a hands-on tutorial-style appendix, where
you take an RxJava-based project and convert it to using coroutines. This repository
represents the results of having completed that tutorial.
This project is updated as the book is, with `git` tags applied to tie code versions to book
versions.

The book, and the samples, were written by Mark Murphy. You may also have run into him through
Stack Overflow:

<a href="https://stackoverflow.com/users/115145/commonsware">
<img src="https://stackoverflow.com/users/flair/115145.png" width="208" height="58" alt="profile for CommonsWare at Stack Overflow, Q&amp;A for professional and enthusiast programmers" title="profile for CommonsWare at Stack Overflow, Q&amp;A for professional and enthusiast programmers">
</a>

All of the source code in this archive is licensed under the
Apache 2.0 license except as noted.
